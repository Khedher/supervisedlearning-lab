This Lab contains 6 parts. The goal is to compelete the TO DO parts.

1. Data reading and splitting.
2. Data visualisation.

For each classifier:

3. Define the classifier parameters
4. Fiting
5. Evaluation

After evaluation all classifiers:

6. Classifier performances comparaison

Classification of Handwritten Digits using Supervised Learning algorithms. This study is carried out MNIST, a dataset of handwritten numerals made of up of 60000 for training and 10000 for test. Each image has a size of 28x28 pixels, the gray level of each being between 0 and 255.

Considered algorithms:

1. Support Vector Machine (SVM)
2. Decision Tree (DT)
3. Random Forest (RF)
4. XGBoost (XGB)
5. Linear Discriminant Analysis (LDA)
